package Automation.Testing;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import Automation.Conection.AndroidCapabilitiesConfig;
import Automation.Conection.URLGenerator;
import Automation.Pages.HomePage;
import Automation.Pages.ProfilePage;
import io.appium.java_client.android.AndroidDriver;


public class TestBase {

  protected static AndroidCapabilitiesConfig androidCapabilities;
  protected static URL url;
  protected static AndroidDriver<WebElement> driver;
  protected HomePage homepage;
  protected ProfilePage profilePage;


  @BeforeSuite
  public void startConection() throws IOException {
	  
	 androidCapabilities = new AndroidCapabilitiesConfig();
	 url = new URL(URLGenerator.getURL());
	 driver = new AndroidDriver<WebElement> (url, androidCapabilities.getCapabilities());
	 driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
  }

  
  @BeforeTest
  public void initPages() {

  }

  @AfterSuite()
  public void tearDown() {
	    driver.quit();
  }
}
