package Automation.Testing;

import java.util.ArrayList;
import org.testng.Assert;
import org.testng.annotations.Test;
import Automation.Pages.HomePage;
import Automation.Pages.ProfilePage;
import Automation.Utils.Gestures;
import Automation.Utils.Helper;



public class TestCases extends TestBase {
	
	  @Test
	  public void testFirstUserProfile() {
		  
		 HomePage homepage = new HomePage(driver);
		 String homeUserName = homepage.getFirstUserName();
		 homepage.selectFirstUser();
			 
		 String profileUserName =  profilePage.getUserName();
		 Assert.assertEquals(homeUserName, profileUserName);
	  }
	  
	  @Test
	  public void testLastUser() {
		  

		 Gestures gesture = new Gestures(driver);
		 gesture.scrollDown();
		 
		 HomePage homepage = new HomePage(driver);
		 String homeUserName = homepage.getLastUserName();
		 homepage.selectLastUser();
			 
		 ProfilePage profilePage = new ProfilePage(driver);
		 String profileUserName =  profilePage.getUserName();
		 Assert.assertEquals(homeUserName, profileUserName);
	  }
	  
	  @Test
	  public void testDuplicatedUsersOnCurrentScreen() {
		  
		 HomePage homepage = new HomePage(driver);
		 ArrayList<String> users = homepage.getUsers();
		 
		 for (int userPosition = 0 ; userPosition < users.size(); userPosition++) {
			 Assert.assertEquals(1, Helper.getMatches(users, users.get(userPosition)), users.get(userPosition) + " is duplicated");
		 }
		 
	  }

}
