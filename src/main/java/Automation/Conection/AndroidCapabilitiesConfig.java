package Automation.Conection;

import org.openqa.selenium.remote.DesiredCapabilities;

public class AndroidCapabilitiesConfig {

	DesiredCapabilities capabilities;


	public AndroidCapabilitiesConfig() {
		
		  
		 capabilities = new DesiredCapabilities();
		   
		 capabilities.setCapability("appActivity", AndroidCapabilitiesData.APP_ACTIVITY);
		 capabilities.setCapability("appPackage", AndroidCapabilitiesData.APP_PACKAGE);
		 capabilities.setCapability("udid", AndroidCapabilitiesData.UI_ID);
		 capabilities.setCapability("platformName", AndroidCapabilitiesData.PLATFORM_NAME);
	}
	
	public DesiredCapabilities getCapabilities() {
		return capabilities;
	}
	
}
