package Automation.Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import io.appium.java_client.android.AndroidDriver;


public abstract class BasePage {

  protected AndroidDriver<WebElement> driver;

  public BasePage(AndroidDriver<WebElement> driver) {
    this.driver = driver;
  }

  public void clickElement(By by) {
	  driver.findElement(by).click();
  }
  
  public void clickElement(WebElement element) {
	  element.click();
  }
  
  public String getText(By by) {
	 return driver.findElement(by).getText();
  }
  
  public String getText(WebElement element) {
	return element.getText();
  }
	  
  public List<WebElement> getListOfElements(By by) {
	  return driver.findElements(by);
  }
  

  
}
