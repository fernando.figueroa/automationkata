package Automation.Pages;


import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import io.appium.java_client.android.AndroidDriver;


public class HomePage extends BasePage {
	 

  private By byUserName = By.id("com.etermax.interviews.smells:id/username");
  

  public HomePage(AndroidDriver<WebElement>  webDriver) {
    super(webDriver);
  }
  
  public String getFirstUserName() {
	  return getText(getListOfElements(byUserName).get(0));
  }
  
  public void selectFirstUser() {
	  
	  clickElement(getListOfElements(byUserName).get(0));
  }

  public String getLastUserName() {
	  
	  int sizeOfList = getListOfElements(byUserName).size();
	  
	  return getText(getListOfElements(byUserName).get(sizeOfList-1));
	
  }

   public void selectLastUser() {
	   
	  int sizeOfList = getListOfElements(byUserName).size();
	  
	  clickElement(getListOfElements(byUserName).get(sizeOfList-1));
  }

	public ArrayList<String> getUsers() {
		
		ArrayList<String> listOfUsers = new ArrayList<String>();
		
		for (int position = 0 ; position < getListOfElements(byUserName).size(); position++){  
			
			listOfUsers.add(getListOfElements(byUserName).get(position).getText());
		}
		
		return listOfUsers;
	}
}
