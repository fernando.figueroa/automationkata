package Automation.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import io.appium.java_client.android.AndroidDriver;

public class ProfilePage extends BasePage {
	
	private By byUserName = By.id("com.etermax.interviews.smells:id/name");

	
	public ProfilePage(AndroidDriver<WebElement>  webDriver) {
	    super(webDriver);
	  }
	
	public String getUserName() {
		return getText(byUserName);
	}
}
