package Automation.Utils;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;

public class Gestures {

private AndroidDriver<WebElement> driver;
	
	public Gestures(AndroidDriver<WebElement> driver){
		this.driver = driver;
	}

	public void scrollDown() {
		
	  Dimension dimension = driver.manage().window().getSize();
	  driver.swipe(0, dimension.height, 0, 0, 1000);

	}
	
}
