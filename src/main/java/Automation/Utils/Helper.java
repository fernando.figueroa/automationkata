package Automation.Utils;

import java.util.ArrayList;

public class Helper {
	
	public Helper() {}

	public static int getMatches(ArrayList<String> list, String valueToSearch) {
		
		int count = 0;
		
		for(int i=0; i< list.size(); i++) {
			if (list.get(i).equals(valueToSearch)) 
				count++;
		}
		
		
		return count;
		
	}
}
